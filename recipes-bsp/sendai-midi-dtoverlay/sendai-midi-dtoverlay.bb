# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# Unable to find any files that looked like license statements. Check the accompanying
# documentation and source headers and set LICENSE and LIC_FILES_CHKSUM accordingly.
#
# NOTE: LICENSE is being set to "CLOSED" to allow you to at least start building - if
# this is not accurate with respect to the licensing of the software being built (it
# will not be in most cases) you must specify the correct value before using this
# recipe for anything other than initial testing/development!
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "file://sendai-midi.dts"

# Modify these as desired
PV = "1.0"

inherit devicetree

FILES:${PN} += "/boot/overlays/sendai-midi.dtbo"



do_install() {
    for DTB_FILE in `ls *.dtb *.dtbo`; do
        install -Dm 0644 ${B}/sendai-midi.dtbo ${D}/boot/overlays/sendai-midi.dtbo
    done
}

do_deploy() {
    for DTB_FILE in `ls *.dtb *.dtbo`; do
        install -Dm 0644 ${B}/sendai-midi.dtbo ${DEPLOYDIR}/overlays/sendai-midi.dtbo
    done
}
COMPATIBLE_MACHINE = "raspberrypi4-sendai"


PACKAGE_ARCH = "${MACHINE_ARCH}"
