SUMMARY = "Simple Xserver Init Script (no dm)"
LICENSE = "CLOSED"
# LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"
SECTION = "x11"

SRC_URI = "file://xinit-sendai.service \
"

S = "${WORKDIR}"

inherit systemd features_check

REQUIRED_DISTRO_FEATURES = "x11 ${@oe.utils.conditional('ROOTLESS_X', '1', 'pam', '', d)}"


do_install () {
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/xinit-sendai.service ${D}${systemd_system_unitdir}
	sed -i -e 's,@SBINDIR@,${sbindir},g' ${D}${systemd_system_unitdir}/xinit-sendai.service
}

SYSTEMD_SERVICE:${PN} = "xinit-sendai.service"

RDEPENDS:${PN} = "xinit ${@oe.utils.conditional('ROOTLESS_X', '1', 'xuser-account libcap libcap-bin', '', d)}"


RCONFLICTS:${PN} = "xserver-common (< 1.34-r9) x11-common"
