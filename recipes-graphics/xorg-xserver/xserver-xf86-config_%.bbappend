FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:sendai = " \
    file://xorg.conf.d/98-pitft.conf \
    file://xorg.conf.d/99-calibration.conf \
"
do_install:append:sendai () {

    install -d ${D}/${sysconfdir}/X11/xorg.conf.d/
    install -m 0644 ${WORKDIR}/xorg.conf.d/98-pitft.conf ${D}/${sysconfdir}/X11/xorg.conf.d/
    install -m 0644 ${WORKDIR}/xorg.conf.d/99-calibration.conf ${D}/${sysconfdir}/X11/xorg.conf.d/

}

FILES:${PN}:append:sendai = " ${sysconfdir}/X11/xorg.conf.d/*"
