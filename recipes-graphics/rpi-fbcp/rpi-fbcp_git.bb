LICENSE = "CLOSED"
# LIC_FILES_CHKSUM = "file://LICENSE;md5=20cd56e60f87e0c4ddd2b6b2d6241e6f"

SRC_URI += "git://github.com/adafruit/rpi-fbcp;protocol=https;branch=master \
           file://0001-Genericize-cmakelists.txt.patch \
           file://0002-add-install-step.patch \
           file://fbcp.service \
           "

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "3a136f2d9803d5b293f6221cc7e3dec03a138648"

DEPENDS = "virtual/libgles2"


S = "${WORKDIR}/git"

inherit cmake systemd

do_install:append () {
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/fbcp.service ${D}${systemd_system_unitdir}
	sed -i -e 's,@SBINDIR@,${sbindir},g' ${D}${systemd_system_unitdir}/fbcp.service
}

SYSTEMD_SERVICE:${PN} = "fbcp.service"
